const INITIAL_STATE = {
  forecast: {
    location:{name:''}, 
    current:{temperature:'',weather_icons:[''], weather_descriptions:['']}, 
  },
  loader: false,
  input: "Lyon"
};

function forecastReducer (state = INITIAL_STATE, action) {
  switch (action.type) {
    case "UPDATE_INPUT":
      return {
        ...state,
        input: action.value,
      };
    case "UPDATE_LOADER":
      return {
        ...state,
        loader: action.status,
      };
    case "UPDATE_FORECAST":
      return {
        ...state,
        forecast: action.value,
      };
    default:
      return state;
  }
}
export default forecastReducer;