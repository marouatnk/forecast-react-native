export const updateInput = (value) => {
    return {
        type: 'UPDATE_INPUT',
        value
    }
  }
  export const updateLoader = (status) => {
    return {
        type : 'UPDATE_LOADER',
        status
    }
  }
  export const updateForecast = (value) => {
    return {
        type: 'UPDATE_FORECAST',
        value
    }
  }
export const updateForecastWithFetched = (value) => {
    return async (dispatch) => {
      console.log(value)
        dispatch(updateLoader(true));
        const response = await fetch('http://api.weatherstack.com/current?access_key=ab8031937a7879113f8efdef4b985c9e&query='+ value);
        const data = await response.json();
        console.log(data)
        dispatch(updateForecast(data));
        dispatch(updateLoader(false));
    }
  }