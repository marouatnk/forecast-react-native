import React from "react";
import { StyleSheet, ImageBackground } from "react-native";
import { Provider } from "react-redux";
import Forecast from "./components/Forecast";
import store from "./store";
import background from "./assets/background.png"
export default function App() {
  return (
    <Provider store={store}>
      <ImageBackground source={background} style={styles.image}>
        <Forecast/>
      </ImageBackground>
    </Provider>
  );
}

const styles = StyleSheet.create({
  image: {
    flex: 1,
    resizeMode: 'cover',
  },
});
