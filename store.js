import thunk from 'redux-thunk';
import { applyMiddleware, combineReducers, createStore } from 'redux';
import  forecastReducer from './reducers/forecastReducer';

const middleware = [thunk];

const store = createStore(
  combineReducers({ forecastReducer }),
  applyMiddleware(...middleware)
);
export default store;
