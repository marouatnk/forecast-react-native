import React from "react";
import ForecastForm from "./ForecastForm";
import ForecastResult from "./ForecastResult";
import ForecastTitle from "./ForecastTitle";
import {
  StyleSheet,
  View,
  KeyboardAvoidingView,
  Platform,
  Keyboard,
  TouchableWithoutFeedback,
} from "react-native";

const Forecast = () => {
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "height"}
      style={{ width: "100%", height: "100%" }}
    >
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={styles.container}>
          <View style={styles.top}>
            <ForecastTitle />
          </View>
          <View style={styles.middle}>
            <ForecastResult />
          </View>
          <View style={styles.bottom}>
            <ForecastForm />
          </View>
        </View>
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
  );
};
const styles = StyleSheet.create({
  container: {
    flexDirection: "column",
    resizeMode: "center",
    width: "100%",
    height: "100%",
  },
  top: {
    flex: 0.3,
    flexDirection: "column",
    width: "100%",
  },
  middle: {
    flex: 0.5,
    flexDirection: "column",
    width: "100%",
  },
  bottom: {

    flex: 0.4,
    flexDirection: "column",
    width: "100%",
    height: "30%",
  },
});

export default Forecast;
