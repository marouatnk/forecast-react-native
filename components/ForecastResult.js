import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { View, Text, Image, StyleSheet } from "react-native";

class ForecastResult extends Component {
  render() {
    if (this.props.forecast.location.name != "") {
      const image = { uri: this.props.forecast.current.weather_icons[0] };
      return (

        <View style={styles.ResultView}>
          <View style={styles.ResultViewTop}>
            <View style={styles.ResultViewTopLeft}>
              <Image alt="Weather" source={image} style={styles.image} />
            </View>
            <View style={styles.ResultViewTopRight}>
              <Text style={styles.weatherDesc}>
                {this.props.forecast.current.weather_descriptions}
              </Text>
            </View>
          </View>
          <View style={styles.ResultViewTop}>
            <View style={styles.ResultViewMiddleLeft}>
              <Text style={styles.weatherTemp}>
                {this.props.forecast.current.temperature} °C
              </Text>
            </View>
            <View style={styles.ResultViewMiddleRight}>
              <Text style={styles.textInfo}>
                Wind Speed: {this.props.forecast.current.wind_speed}
              </Text>
              <Text style={styles.textInfo}>
                Humidity: {this.props.forecast.current.humidity}
              </Text>
            </View>
          </View>
        </View>
      );
    } else {
      return <Text style={styles.searchText}>Choose a City :)</Text>;
    }
  }
}
const mapStateToProps = (state) => {
  return {
    forecast: state.forecastReducer.forecast,
  };
};

function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}
const styles = StyleSheet.create({
  ResultView: {
    width: "90%",
    height: "100%",
    flexDirection: "column",
    backgroundColor: "rgba(255, 255, 255, 2)",
    borderRadius: 12,
    opacity: 0.9,
    margin:"5%"
  },
  ResultViewTop: {
    flex: 1,
    flexDirection: "row",
    left: 10,
    width: "95%",
    borderRadius: 10,
    opacity: 0.9,
  },
  ResultViewTopLeft: {
    flex: 0.4,
    justifyContent: "space-evenly",
    alignItems: "center",
  },
  ResultViewTopRight: {
    flex: 0.7,
    justifyContent: "space-evenly",
    alignItems: "stretch",
  },
  ResultViewMiddleLeft: {
    flex: 0.6,
    alignItems: "center",
  },
  ResultViewMiddleRight: {
    flex: 0.4,
    alignItems: "center",
  },
  weatherTemp: {
    textAlign: "center",
    color: "#104bb4",
    fontSize: 75,
    opacity: 1,
  },
  weatherDesc: {
    textAlign: "left",
    color: "#104bb4",
    fontSize: 50,
    fontWeight: "bold",
    opacity: 1,
  },
  textInfo: {
    marginTop:10,
    fontSize: 20,
    fontWeight: "800",
    color: "black",
  },
  image: {
    width: 90,
    height: 90,
    opacity: 0.8,
  },
  searchText: {
    top: 150,
    fontWeight: "bold",
    textAlign: "center",
    fontSize: 50,
    color: "#FFFFFF",
    justifyContent: "center",
  },
});
export default connect(mapStateToProps, mapDispatchToProps)(ForecastResult);
