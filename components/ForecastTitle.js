import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { View, Text, StyleSheet } from "react-native";

class ForecastTitle extends Component {
  render() {
    const date = Date(this.props.forecast.location.localtime);
    return (
      <View style={styles.titleContainer}>
        <Text style={styles.title}>
          {this.props.forecast.location.name}
        </Text>
        <Text style={styles.date}>{date}</Text>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  titleContainer: {
    top:50,

  },
  date: {
    textAlign:'center',
    fontSize:33,
    color: 'black',
    justifyContent: 'center',
  },
  title: {
    textAlign:'center',
    fontSize:45,
    color: '#FFFFFF',
    justifyContent: 'center',
  },
});
const mapStateToProps = (state) => {
  return {
    forecast: state.forecastReducer.forecast,
  };
};

function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(ForecastTitle);
